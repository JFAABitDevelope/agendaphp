<?php
	
	//Se incluyen las librer�as necesarias
	include("config.php");
	include("dmls.php");
	
	
	//Eavluamos lo que viene por POST y en consecuencia se ejecuta un m�todo u otro
	if(isset($_POST['iBusquedaNombre']) && isset($_POST['iBusquedaApellido']) && isset($_POST['iBusquedaCorreo'])){
		$nombre = $_POST['iBusquedaNombre'];
		$apellido = $_POST['iBusquedaApellido'];
		$correo = $_POST['iBusquedaCorreo'];
		
		if($nombre == '' && $apellido == '' && $correo == ''){
			echo "<div class='dAlerta'><p>No se encontraron personas sin nombre</p></div>";	
		} elseif ($nombre !='' && $apellido == '' && $correo == ''){
			$datos = listaNombre($_POST['iBusquedaNombre']);
		} elseif ($nombre !='' && $apellido != '' && $correo == ''){
			$datos = listaNombreYApellido($_POST['iBusquedaNombre'], $_POST['iBusquedaApellido']);
		} elseif ($nombre !='' && $apellido != '' && $correo != ''){
			$datos = listaNombreApellidoYCorreo($_POST['iBusquedaNombre'], $_POST['iBusquedaApellido'], $_POST['iBusquedaCorreo']);
		} elseif ($nombre !='' && $apellido == '' && $correo != '') {
			$datos = listaNombreYCorreo($_POST['iBusquedaNombre'], $_POST['iBusquedaCorreo']);
		} else{
			$datos = listaApellidoYCorreo($_POST['iBusquedaApellido'], $_POST['iBusquedaCorreo']);
		}
		$twig = config_twig();
		$template = $twig->loadTemplate("lista_encontrados.html");
		echo $template->render(array("datos"=>$datos));
	} else{
		$datoid = listatodosId($_GET['id']);
		$twig = config_twig();
		$template = $twig->loadTemplate("lista_encontrados.html");
		echo $template->render(array("datoid"=>$datoid));
	}
	
	
?>