<?php 
	include ("config.php");
	include ("dmls.php");
	
	if(isset($_GET['id'])){
		$id = getId($_GET['id']);
		$nombre = getNombre($_GET['id']);
		$apellido = getApellido($_GET['id']);
		$telefono = getTelefono($_GET['id']);
		$correo = getCorreo($_GET['id']);
		
		$datos = array('id'=>$id, 'nombre'=>$nombre[0], 'apellido'=>$apellido[0], 'telefono'=>$telefono[0], 'correo'=>$correo[0]);
		$twig = config_twig();
		$template = $twig->loadTemplate("editar.html");
		echo $template->render(array('id'=>$id, "datos"=>$datos));
	} 
	
?>
