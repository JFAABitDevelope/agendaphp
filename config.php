<?php
//Archivo que recoge los datos de configuración de nuestra aplicación

//Datos de administrador de la base de datos MySQL
$root = 'root';
$passwd_admin = 'alumno';


//Función que devuelve un objeto Twig para el manejo de las plantillas
function config_twig(){
	require_once "vendor\autoload.php"; //Autoload.php permite la carga automática
	Twig_Autoloader::register();
	
	//Creamos un objeto Twig_loader_Filesystem para generar una ruta relativa, lo que
	//facilita la portabilidad
	$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)."/vistas/"));
	$twig = new Twig_Environment($loader);
	$escaper = new Twig_Extension_Escaper(true);
	$twig->addExtension($escaper);
	$twig->getExtension('core')->setTimezone('Europe/Madrid');
	return $twig;
}


//Funcion para generar una clave cifrada a partir de una cadena indicada arriba, en este archivo
function generateHash($passwd_admin) {
	if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
		$salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
		return crypt($passwd_admin, $salt);
	}
}

//Almacenamos la clave cifrada
$admin_pass = generateHash($passwd_admin)
?>
