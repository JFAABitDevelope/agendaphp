<?php 
	ini_set("error_reporting", "true");
	error_reporting(E_ALL);

	include ("config.php");
	
	//Aqu� se comprueba si existe sesion de usuario
	session_start();
	
	if (!isset($_SESSION['user'])){
		header("Location: login.php");
	}
	
	$twig = config_twig();
	$twig->addGlobal("session", $_SESSION);
	$template = $twig->loadTemplate("home.html");
	$datos = array('nombre'=>$_SESSION['user']);
	
	//Imprimimos la plantilla. Para pasarle datos a la plantilla, lo hacemos con un array
	echo $template->render($datos);

?>