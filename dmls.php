<?php

//Archivo que gestiona todas las operaciones con la base de datos MySQL

if (isset ( $_POST ['iNombreUsuario'] ) && isset ( $_POST ['iApellidoUsuario'] ) && isset ( $_POST ['iTfnUsuario'] ) 
&& isset ( $_POST ['iCorreoUsuario'] )) {
	insertar ();
}

if (isset ($_POST['iIdEditar']) && isset( $_POST ['iNombreEditar'] ) && isset ( $_POST ['iApellidoEditar'] ) && isset ( $_POST ['iTfnEditar'] ) 
&& isset ( $_POST ['iCorreoEditar'] )) {
	editar ();
}

if (isset ( $_POST ['respuesta'] )) {
	borrartodos ();
}

if (isset($_POST['borrarSeleccionado']) && isset($_POST['id'])) {
	borrarSeleccionado($_POST['id']);
	header("Location: lista_usuarios.php");
}


function insertar() {
	include ("conectarDB.php");
	$nombre = $_POST ['iNombreUsuario'];
	$apellido = $_POST ['iApellidoUsuario'];
	$telefono = $_POST ['iTfnUsuario'];
	$correo = $_POST ['iCorreoUsuario'];
	$sql = "INSERT INTO contactos (Nombre, Apellido, Telefono, Correo) VALUES(
									:Nombre, :Apellido, :Telefono, :Correo)";
	$result = $conexion->prepare ( $sql );
	
	//Utilizamos la tecnología PDO para vincular parámetros con variables
	$result->bindParam ( ':Nombre', ucwords ( $nombre ), PDO::PARAM_STR );
	$result->bindParam ( ':Apellido', ucwords ( $apellido ), PDO::PARAM_STR );
	$result->bindParam ( ':Telefono', $telefono, PDO::PARAM_STR );
	$result->bindParam ( ':Correo', $correo, PDO::PARAM_STR );
	$result->execute ();
	
	$conexion = null;
	
	header ( "Location: lista_usuarios.php" );
}


function editar(){
	include ("conectarDB.php");
	$id = $_POST['iIdEditar'];
	$nombre = $_POST ['iNombreEditar'];
	$apellido = $_POST ['iApellidoEditar'];
	$telefono = $_POST ['iTfnEditar'];
	$correo = $_POST ['iCorreoEditar'];
	
	$sql = "UPDATE contactos SET Nombre=?, Apellido=?, Telefono=?, Correo=? WHERE id=?";
	$result = $conexion->prepare($sql);
	$result->execute(array($nombre, $apellido, $telefono, $correo, $id));
	
	$conexion = null;
	
	header ( "Location: lista_usuarios.php" );
}


function listatodos(){
	include ("conectarDB.php");
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM contactos";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}

function listatodosId($id){
	include ("conectarDB.php");
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$id = $id;
	$sql = "SELECT * FROM contactos where id =".$id;
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}


function listarAsc(){
	include("conectarDB.php");
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM contactos ORDER BY Nombre ASC";
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result;
}


function listarDesc(){
	include("conectarDB.php");
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM contactos ORDER BY Nombre DESC";
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result;
}

function listaNombre($nombre){
	include("conectarDB.php");
	$nom = $nombre.'%';
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM contactos WHERE Nombre LIKE '".$nom."'";
	$result = $conexion->prepare($sql);
	$result->execute();		
	$conexion = null;
	return $result;
}

function listaNombreYApellido($nombre, $apellido){
	include("conectarDB.php");
	$nom = $nombre.'%';
	$ap = $apellido.'%';
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM contactos WHERE Nombre LIKE '".$nom."' AND Apellido LIKE '".$ap."'";
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result;
}
	
	
function listaNombreApellidoYCorreo($nombre, $apellido, $correo){
	include("conectarDB.php");
	$nom = $nombre.'%';
	$ap = $apellido.'%';
	$cor = $correo."%";
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM contactos WHERE Nombre LIKE '".$nom."' AND Apellido LIKE '".$ap."' AND Correo LIKE '".$cor."'";
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result;
}
	
	
function listaNombreYCorreo($nombre, $correo){
	include("conectarDB.php");
	$nom = $nombre.'%';
	$cor = $correo."%";
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM contactos WHERE Nombre LIKE '".$nom."' AND Correo LIKE '".$cor."'";
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result;
}
	
	
function listaApellidoYCorreo($apellido, $correo){
	include("conectarDB.php");
	$ap = $apellido.'%';
	$cor = $correo."%";
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM contactos WHERE Apellido LIKE '".$ap."' AND Correo LIKE '".$cor."'";
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result;
}

	
function colapsaTodos(){
	echo '<script type="text/javascript">';
	echo '$(document).ready(function(){';
	echo '$(".tListaTodos").css("visibility","collapse");';
	echo '$("#aListaAsc").css("visibility","collapse");';
	echo '$("#aListaDesc").css("visibility","collapse");';
	echo '});';
	echo '</script>';
	echo '<a href="lista_usuarios.php" class="aMostrarTodos">Mostrar todos</a>';
}

function colapsaBusqueda(){
	echo '<script type="text/javascript">';
	echo '$(document).ready(function(){';
	echo '$("#tListaNombreYApellido").css("visibility","collapse");';
	echo '});';
	echo '</script>';
}


function borrartodos() {
	include ("conectarDB.php");
	$sql = "DELETE FROM contactos";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
}


function borrarSeleccionado($id) {
	include ("conectarDB.php");
	$sql = "DELETE FROM contactos where id=".$id;
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
}


function getNombre($id){
	include("conectarDB.php");
	$sql = "SELECT Nombre from contactos where id=".$id;
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result->fetch();
}


function getId($id){
	return $id;
}


function getApellido($id){
	include("conectarDB.php");
	$sql = "SELECT Apellido from contactos where id=".$id;
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result->fetch();
}


function getTelefono($id){
	include("conectarDB.php");
	$sql = "SELECT Telefono from contactos where id=".$id;
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result->fetch();
}


function getCorreo($id){
	include("conectarDB.php");
	$sql = "SELECT Correo from contactos where id=".$id;
	$result = $conexion->prepare($sql);
	$result->execute();
	$conexion = null;
	return $result->fetch();
}

function comprueba_passwd($passwd, $cifrada){
	if (function_exists("password_hash")){
		return password_verify($passwd, $cifrada);
	}
	else{
		return crypt($passwd, $cifrada) == $cifrada;
	}
}


function clean($val) {
	return htmlentities(strip_tags(trim($val)), ENT_QUOTES, 'UTF-8');
}


function logout(){
	unset($_SESSION['user']);
	session_destroy();
}









?>