<?php
include("config.php");
include("dmls.php");

if(isset($_POST['borrarTodos'])){
	$decision = $_POST['borrarTodos'];
	if($decision == 'Si'){
		borrartodos();
		header("Location: indez.php");
	} elseif($decision == 'No'){
		header("Location: index.php");
	} 
}else{
	$twig = config_twig();
	$template = $twig->loadTemplate("confirmar_borrar.html");
	echo $template->render(array());
}

