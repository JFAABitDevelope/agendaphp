<?php
	ini_set("error_reporting", "true");
	error_reporting(E_ALL);
	include('config.php');
	include("dmls.php");
	$datos ="";
	
	if(isset($_GET['asc'])){
		$datos = listarAsc();
	} elseif(isset($_GET['desc'])){
		$datos = listarDesc();
	} 
	
	$twig = config_twig();
	$template = $twig->loadTemplate("lista_encontrados.html");
	echo $template->render(array("datos"=>$datos));
?>