<?php
	include 'config.php';
	include 'dmls.php';

	if (isset($_POST['nombreLogin']) and isset($_POST['passwordLogin'])){
		$user = clean($_POST['nombreLogin']);
		$passwd = clean($_POST['passwordLogin']);
		
		if ($user == "root" and comprueba_passwd($passwd, $admin_pass)){
			session_start();
			$_SESSION['user'] = 'root';
			header ("Location: index.php");
		} else{
			header ("Location: login.php");
		} 
	} else {
		$twig = config_twig();
		$template = $twig->loadTemplate('login.html');
		echo $template->render(array());
	}
?>