<?php
session_start();

ini_set("error_reporting", "true");
error_reporting(E_ALL | E_STRICT);

include "config.php";
include "dmls.php";

logout();
header("Location: index.php");

?>